# Contact Formatter

Contact Formatter is a module designed to provide a field formatter for 
Drupal's Contact Forms. It enhances the presentation and display of contact 
form fields, ensuring a more streamlined and user-friendly experience for site 
administrators and visitors alike.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/contact_formatter).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/contact_formatter).


## Table of contents

- Requirements
- Recommended modules
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further 
information, see 
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module at Administration > Extend.
2. Configure the desired field formatter settings at Administration > 
   Configuration > Content authoring > Contact forms.


## Maintainers

- Jim Birch - [thejimbirch](https://www.drupal.org/u/thejimbirch)
- Albert Jankowski - [albertski](https://www.drupal.org/u/albertski)
- Michael Porter - [michaelpporter](https://www.drupal.org/u/michaelpporter)
- Mike Acklin - [mikeacklin](https://www.drupal.org/u/mikeacklin)
